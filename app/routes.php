<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('hello');
});

Route::resource('carteleras', 'CartelerasController');
Route::resource('cines', 'CinesController');
Route::resource('formatos', 'FormatosPeliculasController');
Route::resource('peliculas', 'PeliculasController');
Route::resource('salas', 'SalasController');
Route::resource('tiposdesalas', 'TiposSalasController');
Route::get('salascine/{id}', 'SalasController@salasPorCineId');
Route::get('peliculascine/{id}', 'CartelerasController@peliculaCineId');
