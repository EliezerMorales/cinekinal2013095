<?php

class CartelerasController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Cartelera::all();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Cartelera::find($id);
    }
    /**
     * Display the specified resource.
     * @param  int  $id
     * @return Response
     */
    public function peliculaCineId($id)
    {
        /*$pelicula=Cine::find($id)->sala;
        return $pelicula;*/
        $pelicula=DB::table('Cine')
            ->join('Sala', 'Cine.id', '=', 'Sala.cine_id')
            ->join('Cartelera', 'Sala.id', '=', 'Cartelera.sala_id')
            ->join('Pelicula', 'Cartelera.pelicula_id', '=', 'Pelicula.id')
            ->select('Pelicula.id', 'Pelicula.titulo', 'Pelicula.sinopsis', 'Pelicula.trailer_url', 'Pelicula.rated', 'Pelicula.genero')
            ->where('Cine.id', '=', $id)->get();

        return $pelicula;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
