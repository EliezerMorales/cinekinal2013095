<?php
/**
 * Created by PhpStorm.
 * User: eliezer
 * Date: 19/07/15
 * Time: 12:13 PM
 */
class Cine extends Eloquent{
    protected $table = 'Cine';

    public function sala(){
        return $this->hasMany('Sala');
    }

}