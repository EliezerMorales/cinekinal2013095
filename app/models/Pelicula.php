<?php
/**
 * Created by PhpStorm.
 * User: eliezer
 * Date: 19/07/15
 * Time: 12:14 PM
 */
class Pelicula extends Eloquent{
    protected $table = 'Pelicula';

    public function cartelera(){
        return $this->hasMaby('Cartelera');
    }

}