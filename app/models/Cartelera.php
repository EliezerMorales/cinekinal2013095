<?php
/**
 * Created by PhpStorm.
 * User: eliezer
 * Date: 19/07/15
 * Time: 12:12 PM
 */
class Cartelera extends Eloquent{
    protected $table = 'Cartelera';

    public function sala(){
        return $this->belongsTo('Sala', 'sala_id');
    }
    public function pelicula(){
        return $this->belongsTo('Pelicula', 'pelicula_id');
    }
    public function formato(){
        return $this->belongsTo('FormatoPelicula', 'formatopelicula_id');
    }
}

