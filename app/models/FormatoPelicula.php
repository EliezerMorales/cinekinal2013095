<?php
/**
 * Created by PhpStorm.
 * User: eliezer
 * Date: 19/07/15
 * Time: 12:14 PM
 */
class FormatoPelicula extends Eloquent{
    protected $table='FormatoPelicula';

    public function peliculas(){
        return $this->hasMany('Cartelera');
    }
}