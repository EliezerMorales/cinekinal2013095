<?php
/**
 * Created by PhpStorm.
 * User: eliezer
 * Date: 19/07/15
 * Time: 12:14 PM
 */
class Sala extends Eloquent{
    protected $table = 'Sala';

    public function cine(){
        return $this->belongsTo('Cine', 'cine_id');
    }

    public function cartelera(){
        return $this->hasMany('Cartelera');
    }
}